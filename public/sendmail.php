<?php

require '../vendor/autoload.php';
use Mailgun\Mailgun;

$name    = $_POST["name"];
$email   = $_POST["email"];
$message = $_POST["message"];

# Instantiate the client.
$mgClient = new Mailgun('key-6bde4d5b557438e856e69a0f27650655');
$domain = "mg.dangpaddles.com";

// Make the call to the client.
$result = $mgClient->sendMessage($domain, array(
    'from'    => $email,
    'to'      => 'dangpaddles@gmail.com',
    'subject' => 'Order Inquiry for ' . $name,
    'text'    =>  $message
));

$response = array();
$response['status'] = '200';
echo json_encode($response);
$(document).ready(function() {

    $('.bg-main').addClass("img-" + Math.floor((Math.random() * 3) + 1));

    setTimeout(function(){
        $('body').addClass('loaded');
    }, 1500);
    setTimeout(function() {    
        $('.bg').addClass('animated fadeIn').fadeIn('slow');
        $('section').addClass('animated slideInDown').fadeIn('slow');
    }, 1700);

    $('#closeBtn').click(function () {
    	$('.overlay').removeClass('animated fadeInUpBig').addClass('animated fadeOutDownBig').fadeOut('slow');
    	$('.bg').removeClass('animated fadeOut').addClass('animated fadeIn').fadeIn('slow');
        $('section').removeClass('animated slideOutUp').addClass('animated slideInDown').fadeIn('slow');
        $('.footer').fadeIn('slow');
    });

    $('#titleBtn').click(function () {
        $('.bg').removeClass('animated fadeIn').addClass('animated fadeOut').fadeOut('slow');
        $('.overlay').removeClass('animated fadeOutDownBig').addClass('animated fadeInUpBig').fadeIn('slow');
        $('section').removeClass('animated slideInDown').addClass('animated slideOutUp').fadeOut('slow');
        $('.footer').fadeOut('slow');
    });

    $('.contact-form form').submit(function () {
    	$.ajax({
    		type: 'POST',
    		url: 'public/sendmail.php',
    		data: {
    			name: $('#name').val(),
    			email: $('#email').val(),
    			message: $('#message').val(),
    		},
            dataType: 'JSON',
            success: function(data) {
                if (data['status'] == 200) {
                    $('.message').addClass('animated fadeIn').html("Email sent successfully!");
                };
            },
    	});
        return false;
    });
});
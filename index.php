<html>

	<head>
    <meta charset="utf-8">
    <meta content='width=device-width, initial-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="author" content="Kevin Em">
    <meta name="author" content="Steven Em">
    <meta name="author" content="Stephen Em">

    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
    <link href='https://fonts.googleapis.com/css?family=Roboto:400,100,500,700,900,300' rel='stylesheet' type='text/css'>  
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="public/css/animate.css">
    <link rel="stylesheet" href="public/css/style.css">
    <link rel="stylesheet" href="public/css/preloader.css">
    <link rel="shortcut icon" href="public/favicon.ico">

    <title>Dang Paddles</title>
  </head>

	<body>
      <!-- Google Analytics -->
      <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
        ga('create', 'UA-43671691-1', 'auto');
        ga('send', 'pageview');
      </script>

      <!-- Preloader -->
      <div id="loader-wrapper">
          <div id="loader"></div>

          <div class="loader-section section-left"></div>
          <div class="loader-section section-right"></div>
       
      </div>

      <!-- Background -->
      <div class="bg bg-main"></div>

      <div class="bg bg-black"></div>

      <section class="col-sm-6 col-sm-offset-3">
        <h1 id="title">Dang Paddles</h1>
        <h4 id="description">Carpentry | Artistry | Craft </h4>
        <a id="titleBtn" class="btn btn-circle">
          <i class="fa fa-angle-double-up"></i>
        </a>
        <div class="text-center">
          <ul class="list-inline">
            <li>
              <a href="https://www.facebook.com/DangPaddles" target="_blank">
                <i class="fa fa-facebook"></i>
              </a>
            </li>
            <li>
              <a href="https://instagram.com/dangpaddles/?hl=en" target="_blank">
                <i class="fa fa-instagram"></i>
              </a>
            </li>
          </ul>
        </div>
      </section>

      <!-- Footer -->
      <footer class="footer">
        <p>Dang Paddles &copy; 2015</p> 
      </footer>

      <div class="overlay container-fluid">
        <div class="row">
          <h1 class="form-title">Contact</h1>
          <p class="text-center form-description">
          Want to ask a question or get a quote? Shoot us an email and we will be sure to get back to you soon.<br>
          Please include the following details if you are ordering.<br>
          </p>

          <div class='contact-form col-sm-6 col-sm-offset-3'>
            <form action="public/sendmail.php" method="post" enctype="multipart/form-data">
              <div class="form-group">
                <label for="name">Name</label>
                <span class="form-required">*</span>
                <input type="text" class="form-control" name="name" id="name" required>
              </div>
              <div class="form-group">
                <label for="email">Email</label>
                <span class="form-required">*</span>
                <input type="email" class="form-control" name="email" id="email" required>
              </div>
              <div class="form-group">
                <label for="topDesign">Top Design</label>
                <input type="text" class="form-control" name="topDesign">
              </div>
              <div class="form-group">
                <label for="middlePortion">Middle Portion</label>
                <input type="text" class="form-control" name="middlePortion">
              </div>
              <div class="form-group">
                <label for="woodenLetters">Wooden Letters</label>
                <input type="text" class="form-control" name="woodenLetters">
              </div>
              <div class="form-group">
                <label for="phoneNumber">Phone Number</label>
                <input type="phone" class="form-control" name="phoneNumber">
              </div>
              <div class="form-group">
                <label for="address">Address</label>
                <input type="text" class="form-control" name="address"
                  placeholder="Located in Riverside, CA. Pickup is an option">
              </div>
              <div class="form-group">
                <label for="message">Message</label>
                <span class="form-required">*</span>
                <textarea class="form-control" name="message" id="message" rows="5" placeholder="Get Schwifty!" required></textarea>
              </div>
              <p class="message"></p>
              <input type="submit" class="btn btn-default" value="Send">
            </form>
          </div>
          <div class="text-center col-sm-12">
            <a id="closeBtn" class="btn btn-circle">
              <i class="fa fa-angle-double-down animate"></i>
            </a>
          </div>
        </div>
      </div>

    <script src="public/js/jquery.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
    <script src="public/js/animate.js"></script>
	</body>

</html>